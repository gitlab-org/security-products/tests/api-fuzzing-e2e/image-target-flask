FROM python:3.8

RUN \
    apt-get update \
    && apt-get install -y \
    build-essential

EXPOSE 7777
EXPOSE 7778

ENV TZ="America/Los_Angeles"

WORKDIR /app
ADD . /app

RUN python -m pip install -r requirements.txt

#
# CWE Shellshock requires an outdated bash version
# RUN wget https://snapshot.debian.org/archive/debian/20130101T091755Z/pool/main/b/bash/bash_4.2%2Bdfsg-0.1_amd64.deb -O /tmp/bash_4.2+dfsg-0.1_amd64.deb \
#     && dpkg -i /tmp/bash_4.2+dfsg-0.1_amd64.deb
RUN apt-get install libtinfo5 \
    && dpkg -i /app/packages/bash_4.2+dfsg-0.1_amd64.deb

CMD python /app/rest_target.py
